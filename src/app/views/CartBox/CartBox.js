import classes from "./CartBox.module.scss";
import BasicLayout from "../../Layout/Basic.layout";
import { useEffect, useState } from "react";
import { Helper } from "scriptpack";
import {
  AiOutlineMinus,
  AiOutlinePlus,
  BsTrash,
  FcFullTrash,
} from "react-icons/all";
import { Link } from "react-router-dom";
import { Constants } from "../../values/Constants";
import { useProduct, useProductAction } from "../../utils/StateManagerProduct";
import { useParams } from "react-router";

function CartBox(props) {
  console.log(props);
  const [cart, setCart] = useState([]);
  const { id } = useParams();
  const dispatch = useProductAction();
  const product = useProduct();

  // useEffect(() => {
  //   dispatch({ type: "singleProduct", value: +id });
  // }, [id]);

  return (
    <BasicLayout>
      <div className={classes.Container}>
        {cart?.map((item) => (
          <div key={item.id} className={classes.Product}>
            <Link to={`/productDetails/${item.id}`}>
              <img className={classes.Image} src={item.source} alt="" />
            </Link>
            <div className={classes.Body}>
              <h2 className={classes.Title}>{item.title}</h2>
              <div className={classes.BoxPrice}>
                <span className={classes.Text}>قیمت</span>
                <span className={classes.Price}>
                  {Helper.toCurrencyFormat(item.price)}
                </span>
              </div>
              <p className={classes.BoxColor}>
                <span className={classes.Text}>رنگ</span>
                <span
                  className={classes.Color}
                  style={{ backgroundColor: item.color }}
                ></span>
              </p>
              <div className={classes.PlusMines}>
                <button
                  className={classes.Plus}
                  onClick={() => {
                    dispatch({ type: "increment", value: item.id });
                  }}
                >
                  <AiOutlinePlus />
                </button>
                <span className={classes.Quantity}>{item.qty}</span>
                <button
                  className={classes.Mines}
                  onClick={() => {
                    dispatch({ type: "decrement", value: item.id });
                  }}
                >
                  <AiOutlineMinus />
                </button>
              </div>
            </div>
            <button
              onClick={() => {
                dispatch({ type: "delete", value: item.id });
              }}
              className={classes.Trash}
            >
              <BsTrash />
            </button>
          </div>
        ))}
      </div>
    </BasicLayout>
  );
}
export default CartBox;
